## Weekly Priorities
This content is meant to communicate my priorities on a weekly basis. They should
typically be reflected as items in the `Doing` of
[my personal issue board](https://gitlab.com/groups/gitlab-com/-/boards/1702046?assignee_username=ebrinkman).
It will be updated weekly. I may also include a priority that is not work related,
work related but silly in nature, or a learning goal depending on the week. Most of my
weeks are pre-planned due to recurring meetings and 1x1s so in most cases my weekly priorities
listed will be items that are above and beyond this pre-planned work.

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **P** - Partially Completed
* **N** - Not completed

## 2020-01-11
1. Updates from Product Offsite
1. PI Review Meeting
1. Define Q1 OKRs

## 2020-01-04
1. Y - [Discuss items on settings and required jobs with Sid and team](https://docs.google.com/document/d/1nMJzrDfG7C14WP5v7P226oPFuXkwqIk7bdIT8ai0DNU/edit?ts=5fdbd4f2#bookmark=id.l86rkqxeifa1)
1. Y - [Product Leadership Offsite](https://docs.google.com/document/d/10eBgoH1WvVdVmQAD77hlQqXk1wruc5TpLECb3hMKHsk/edit)


## 2020-12-28 (Vacation)

## 2020-12-21 (Vacation)

## 2020-12-14
1. [Update Dev Direction based on FY22 Themes](https://gitlab.com/gitlab-com/Product/-/issues/1841)
1. [Weekly Opp Review](https://gitlab.com/gitlab-com/Product/-/issues/1850)
1. [13.8 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1826)
1. [Top ARR Driver Review with Sales](https://docs.google.com/document/d/1TxcJqOPWo4pP1S48OSMBnb4rysky8dRrRWJFflQkmlM/edit#)
1. [Dev Section PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1811)

## 2020-12-07
1. Y - [DIB training](https://gitlab.com/gitlab-com/Product/-/issues/1730)
1. Y - [Weekly Opp Review](https://gitlab.com/gitlab-com/Product/-/issues/1791)
1. Y - [Review GitHub Public Roadmap](https://gitlab.com/gitlab-com/Product/-/issues/1822)
1. Y - [Review Industry Code Review Docs](https://gitlab.com/gitlab-com/Product/-/issues/1823)

## 2020-11-30
1. Y - Dig out from e-mail/To Dos from Thanksgiving Break
1. P - [DIB training](https://gitlab.com/gitlab-com/Product/-/issues/1730)
1. Y - [Weekly Opp Review](https://gitlab.com/gitlab-com/Product/-/issues/1791)
1. P - [Review Dev Section Category Direction Pages](https://gitlab.com/gitlab-com/Product/-/issues/1752)

## 2020-11-23 (Thanksgiving Break)
1. Thanksgiving Break

## 2020-11-16
1. Y - [Prep and hold PI review next week](https://gitlab.com/gitlab-com/Product/-/issues/1719)
1. Y - [Prep for Product Key meeting](vhttps://gitlab.com/gitlab-com/Product)
1. N - [Begin DIB training](https://gitlab.com/gitlab-com/Product/-/issues/1730)
1. Y - [Hold GitLab 13.7 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1704)

## 2020-11-09 (OoO on Friday)
1. P - [Prep for PI review next week](https://gitlab.com/gitlab-com/Product/-/issues/1719)
1. Y - Attend 2+ hours of QBRs (see calendar for invites)
1. Y - [Ensure clarity and meet with Sid on Instance Groups](https://gitlab.com/groups/gitlab-org/-/epics/4257)
1. Y - [Dev Direction Page Updates](https://gitlab.com/gitlab-com/Product/-/issues/1720)

## 2020-11-02
1. Y - [Execute realignments](https://gitlab.com/gitlab-com/Product/-/issues/1698)
1. Y - [Finish Gitaly Cluster tiering exercise](https://gitlab.com/gitlab-com/Product/-/issues/1669)
1. Y - CDF Refresh for Jeremy
1. Y - CDF Refresh for EricS

## 2020-10-26
1. N - [Attempt to use low-code platform to create an app](https://gitlab.com/gitlab-com/Product/-/issues/1604)
1. N - [Update Dev Direction page for GC content](https://gitlab.com/gitlab-com/Product/-/issues/1673)
1. P - [Review weekly top opportunities](https://gitlab.com/gitlab-com/Product/-/issues/1671)
1. Y - CDF Refresh for Justin Farris
1. P - [Eliminate confusion over Gitaly Cluster tiering](https://gitlab.com/gitlab-com/Product/-/issues/1669)

## 2020-10-19
1. N - [Attempt to use low-code platform to create an app](https://gitlab.com/gitlab-com/Product/-/issues/1604)
1. Y - [Prep and present 13.6 kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1599)

## 2020-10-12 (3 Day Work Week)
1. N - [Attempt to use low-code platform to create an app](https://gitlab.com/gitlab-com/Product/-/issues/1604)
1. Y - Align on Settings and Navigatin Work(https://gitlab.com/gitlab-com/Product/-/issues/1623)
1. Y - Have a great Monday and Friday off!

## 2020-10-05
1. N - [Attempt to use low-code platform to create an app](https://gitlab.com/gitlab-com/Product/-/issues/1604)
1. Y - Prep and hold important partner meeting
1. Y - Have a great Friday off for Friends and Family Day!

## 2020-09-21
1. N - [Finish Dev Section Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1485)
1. N - [Begin research of low/no code by merging in MVC of Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1480)
1. P - [Align on MVC for Instance Groups/Group Admin Menu](https://gitlab.com/gitlab-com/Product/-/issues/1550)
1. Y - [Prep and and present at IACV drivers meeting](https://gitlab.com/gitlab-com/Product/-/issues/1551)

## 2020-09-14
1. N - [Finish Dev Section Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1485)
1. Y - [Hold Dev Section GC](https://gitlab.com/gitlab-com/Product/-/issues/1508)
1. Y - [Prep and Present 13.5 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1502)

## 2020-09-07
1. N - [Finish Dev Section Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1485)
1. Y - [Hold PI review next week](https://gitlab.com/gitlab-com/Product/-/issues/1486)
1. Y - [Begin Prep for Dev Section GC](https://gitlab.com/gitlab-com/Product/-/issues/1508)
1. N - Continue working through CI refresher class

## 2020-08-31
1. Y - [August Dev Walkthrough](https://gitlab.com/gitlab-com/Product/-/issues/1444)
1. Y - Hold discussion of part 2 of Better Allies
1. P - [Finalize Dev Section Dashboard](https://gitlab.com/gitlab-com/Product/-/issues/1485)
1. Y - [Begin Prep for PI review next week](https://gitlab.com/gitlab-com/Product/-/issues/1486)
1. N - Continue working through CI refresher class

## 2020-08-24
1. Y - [Help to harmonize the Plan team](https://gitlab.com/gitlab-org/plan/-/issues/148)
1. Y - [Organize usability issues I'd like to fix](https://gitlab.com/gitlab-com/Product/-/issues/1470)
1. N - [August Dev Walkthrough](https://gitlab.com/gitlab-com/Product/-/issues/1444)
1. Y - Attend GitLab Commit

## 2020-08-17
1. N - [August Dev Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/1444)
1. N - [August Dev Walkthrough](https://gitlab.com/gitlab-com/Product/-/issues/1444)
1. Y - [GitLab 13.4 Kickoff Presentation](https://gitlab.com/gitlab-com/Product/-/issues/1418)
1. Y (via @justinfarris) [Play with GitHub automation and record walkthrough](https://gitlab.com/gitlab-com/Product/-/issues/1420)
1. Y - Successfully get kids up and running for their first week of virtual school!

## 2020-08-10
1. Y - [Host Dev PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1400)
1. Y - [Finish work on investment case](https://gitlab.com/gitlab-com/Product/-/issues/1407)
1. N - [Play with GitHub automation and record walkthrough](https://gitlab.com/gitlab-com/Product/-/issues/1420)
1. Y - [Prep for James Ramsay's baby shower next Monday](https://gitlab.com/gitlab-com/Product/-/issues/1421)

## 2020-08-03
1. Y - [Finalize Prep for Dev PI Review](https://gitlab.com/gitlab-com/Product/-/issues/1400)
1. Y - [Host a great Dev section group conversation](https://gitlab.com/gitlab-com/Product/-/issues/1367) 
1. Y - [Finalize Q3 OKR scope](https://gitlab.com/gitlab-com/Product/-/issues/1320)
1. Y - General catch up from vacation

## 2020-07-27
1. Y - Enjoy Vacation

## 2020-07-20
1. Y - [Shore up Dev Section Dashboards/PI Page](https://gitlab.com/gitlab-com/Product/-/issues/1360)
1. N - [Learning: Complete Udemy CI/CD course for refresher](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/learn/lecture/15119524?start=0#overview)

## 2020-07-13
1. Y - [Work to define Q3 Product OKRs](https://gitlab.com/gitlab-com/Product/-/issues/1320)
1. Y - [Update Dev Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1251)
1. Y - [Begin work on creating Dev Section dashboards](https://gitlab.com/gitlab-com/Product/-/issues/1282)
1. P - [Learning: Complete Udemy CI/CD course for refresher](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/learn/lecture/15119524?start=0#overview)

## 2020-07-06
1. Y - Enjoy Vacation

## 2020-06-29
1. Y - [Prep for key partner meeting](https://gitlab.com/gitlab-com/Product/-/issues/1283)
1. Y - [Create slides and deliver talk for CodeUp students](https://gitlab.com/gitlab-com/Product/-/issues/1284)
1. N - [Begin work on creating Dev Section dashboards](https://gitlab.com/gitlab-com/Product/-/issues/1282)
1. Y - [Update README with 360 feedback and development themes](https://gitlab.com/gitlab-com/Product/-/issues/1281)
1. Y - [Dev PI meeting](https://gitlab.com/gitlab-com/Product/-/issues/1263)
1. N - [Update Dev Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1251)
1. Y - [Learning: Finish reading Competing with Luck](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer/dp/0062435612) (40 pages to go)
1. N - [Learning: Complete Udemy CI/CD course for refresher](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/learn/lecture/15119524?start=0#overview)

## 2020-06-22
1. Y - [Host Dev Section Group Conversation](https://gitlab.com/gitlab-com/Product/-/issues/1158)
1. P - [Deep dive into product metrics and prep for Dev Section Peformance Indicator Review](https://gitlab.com/gitlab-com/Product/-/issues/1263)
1. P - [Learning: Continue reading Competing with Luck](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer/dp/0062435612)

## 2020-06-15
1. Y - [Plan and present 13.2 kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1227)
1. P - [Complete 360 Feedback](https://gitlab.com/gitlab-com/Product/-/issues/1210)
1. Y - [Align on resourcing for 13.2 Jira integration work](https://gitlab.com/gitlab-com/Product/-/issues/1237)
1. N (meeting was moved) - [Review IACV drivers sheet and update items](https://gitlab.com/gitlab-com/Product/-/issues/1239)
1. Y - [Prep for Dev Section Group Conversation](https://gitlab.com/gitlab-com/Product/-/issues/1158)
1. N - [Learning: Continue reading Competing with Luck](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer/dp/0062435612)

## 2020-06-08
1. Y - [Align on Atlassian Strategy](https://gitlab.com/gitlab-org/gitlab/-/boards/1789772?label_name[]=atlassian)
1. P - [Complete 360 Feedback](https://gitlab.com/gitlab-com/Product/-/issues/1210)
1. N - [Learning: Continue reading Competing with Luck](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer/dp/0062435612)

## 2020-06-01 (3 day work week)
1. Y - [Update Dev Direction Page for May/June](https://gitlab.com/gitlab-com/Product/-/issues/1176)
1. N - [Complete 360 Feedback](https://gitlab.com/gitlab-com/Product/-/issues/1210)
1. Y - [Review Dev Section Product Scaling Agenda Items](https://gitlab.com/gitlab-com/Product/-/issues/1211)
1. Y - [Learning: Continue reading Competing with Luck](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer/dp/0062435612)

## 2020-05-25 (3 day work week)
1. N - [Update Dev Direction Page for May](https://gitlab.com/gitlab-com/Product/-/issues/1176)
1. Y - [Conduct 13.0 walkthrough](https://gitlab.com/gitlab-com/Product/-/issues/1191)
1. Y - [Learning: (Finally Finally) Finish Accelerate](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339)

## 2020-05-18
1. Y - [Present 13.1 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1156)
1. Y - [Prep for Product Key Meeting](https://gitlab.com/gitlab-com/Product/-/issues/1148)
1. N - [Update Dev Direction Page for May](https://gitlab.com/gitlab-com/Product/-/issues/1176)
1. Y - [Dive into Inbox Analytics](https://gitlab.com/groups/gitlab-org/-/epics/3061)
1. N - [Learning: (Finally) Finish Accelerate](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339)

## 2020-05-11
1. Y - [Host Dev Section Group Conversation](https://gitlab.com/gitlab-com/Product/-/issues/1137)
1. Y - [Host Dev Section North Star Metric Review](https://gitlab.com/gitlab-com/Product/-/issues/1139)
1. Y - [Align on server runtime plans](https://gitlab.com/gitlab-com/Product/-/issues/1155)
1. Y - [Prep for 13.1 Kickoff](https://gitlab.com/gitlab-com/Product/-/issues/1156)
1. N - [Learning: Finish Accelerate](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339)

## 2020-05-04
1. Y - [Prep for the Dev Section Group Conversation](https://gitlab.com/gitlab-com/Product/-/issues/1137)
1. Y - [Finish CDF Refreshes](https://gitlab.com/gitlab-com/Product/-/issues/1138)
1. Y - [Prep for the Dev Section North Star Metric Review](https://gitlab.com/gitlab-com/Product/-/issues/1139)
1. Y - [Learning: Setup a sample project on Glitch](https://gitlab.com/gitlab-com/Product/-/issues/1134)
1. N - [Learning: Finish Accelerate](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339)


## 2020-04-27
1. Y - [Complete Forrester VSM Wave Process (Questionnaire, Survey, and Briefing)](https://gitlab.com/groups/gitlab-com/marketing/-/epics/806)
1. Y - [Update the Dev Direction Page](https://gitlab.com/gitlab-com/Product/-/issues/1112)
1. Y - [Prep and participate in Gartner IT Value Stream Inquiry](https://gitlab.com/gitlab-com/marketing/product-marketing/-/issues/2255)
1. N (but was worked on) - [Align on Quality Management MVC](https://gitlab.com/gitlab-com/Product/-/issues/1113)
1. Y - [Finalize Q1 OKR activites](https://gitlab.com/groups/gitlab-com/-/epics/262)
1. Y - [Fun: Hold a Dev Section Pizza Party!](https://gitlab.com/gitlab-com/Product/-/issues/1114)
